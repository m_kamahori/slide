gnuplot <<EOF
set term svg fsize 17
set logscale
# set xr[100:100000]
# set xl "Number of elements"
# set yl "Computational time [sec]"
set out "./time_ne_srr.svg"
p "time_ne.dat_amat" w lp notitle ,\
  "time_ne.dat_wood" w lp notitle ,\
  "time_ne.dat_hamat" w lp notitle ,\
  "time_ne.dat_hwood" w lp notitle

EOF
