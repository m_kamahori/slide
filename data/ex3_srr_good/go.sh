DIR=`pwd`
cd ../../../hbandopt2/src
cp makefile_conv makefile
make clean
make
cd ../exec
rm history.dat
step1=0;step2=50;mflag=0
xe=60;eps=8;tol=12
for m in 4 8 16 32 64 128 256 512
do
    for islv in -1 0 2 3
    do
	echo "m=${m}, islv=${islv}"
	if [ $islv -eq -1 ];then
	    out=${DIR}/time_ne.dat_amat
	elif [ $islv -eq 0 ];then
	    out=${DIR}/time_ne.dat_wood
	elif [ $islv -eq 2 ];then
	    out=${DIR}/time_ne.dat_hwood
	elif [ $islv -eq 3 ];then
	    out=${DIR}/time_ne.dat_hamat
	fi
	touch ${out}
	rm time_ne.dat
	if [ $step1 -eq 0 ];then
	    rm band/*.bnd bp/*.bp lsf/*.ls2 ip2/*.ip2 ip/*.ip
	    if [ $mflag -eq 0 ];then
		cd mesh && gfortran srr.f90 && echo $m | ./a.out&& cd -
		# mesher=square.f90
		# cd mesh && gfortran $mesher && echo 1280 40  | ./a.out&& cd -
	    fi
	fi
	# cd ../src && make && cd ../exec
	echo 32                !threadの数 > input
	echo $mflag           !初期形状を0:外部プログラムで生成，1:内部プログラムで生成。 >> input
	echo $xe              !x yousosu >> input # 偶数のみ
	echo $xe              !y yousosu >> input
	echo 1.d0             !a_1 no okisa >> input
	echo 1.d0             !a_2 no okisa >> input
	echo 9.d1             !a_1 to a_2 no nasu kaku >> input
	echo 2                !0:assymmetry, 1:1/4 symmetry, 2:1/8 symmetry >> input
	echo 0                !0:gamma点なし, 1:gamma点あり >> input
	echo 1.d-1            !ana no hankei >> input
	echo ${islv}	      !-1:amat, 0:woodbury, 1:hbmat only, 2:hmat_woodbury, 3:hamat >> input
	echo 100	      !ev no saidai kosu >> input
	echo 32              !nskbn:積分点の数 >> input
	# echo 1                !sekibun en no kazu >> input
	# echo 1.6d0 0.d0       !1st sekibun en no chusin >> input
	# echo 2.d0             !rad:積分円の半径 >> input
	# echo 0.5d0            !margin:積分円のマージン >> input
	echo 1                !sekibun en no kazu >> input
	echo 4.026d0 0.d0       !1st sekibun en no chusin >> input
	echo 1.d-1             !rad:積分円の半径 >> input
	echo 0.005d0            !margin:積分円のマージン >> input
	echo 5                !nblck:blockの数、固有値の重複度がnblck以下になるようにしておく >> input
	echo 10               !nhank:Hankel行列のサイズ >> input
	echo $step1           !start step >> input
	echo $step2           !end step >> input
	echo 2                !1youso no bunkatsusu >> input
	echo 1                !chiisaku shitai mode >> input
	echo 2                !okiku shitai mode >> input
	echo 0.d0             !offset1 >> input
	echo 0.08d0           !offset2 >> input
	echo 5.d-2            !拡散方程式時間差分の幅 >> input
	echo 1.d-2            !拡散方程式時間差分の幅 >> input
	echo 5                !tloop1:どのステップまでdeltat1で計算するか >> input
	echo 6                !tloop2:どのステップからdeltat2で計算するか >> input
	echo 1.d0             !prm_c:1d0で固定 >> input
	# echo 1.d-5            !複雑度係数 >> input
	echo 5.d-4            !複雑度係数 >> input
	# echo 5.d-3            !複雑度係数 >> input
	# echo 8.d-7            !複雑度係数 >> input
	echo 32                !nmin >> input
	echo 32                !nminin >> input
	echo 1.d0              !eta: admissible conditionのeta >> input
	echo 1.d0              !eta_in: 内点のeta >> input
	echo 1.d-${eps}            !eps_aca: ACAの許容誤差 >> input
	echo 1.d-${tol}            !tol: H行列計算の許容誤差 >> input
	echo 0                !cflag: agglomerationするかしないか >> input
	echo 1.d-4            !ceps: agglomerationの許容誤差 >> input
	./a.out < input>>${DIR}/log.dat
	cat time_ne.dat >> ${out}
	# cp time.dat ${DIR}/time.dat
	# cp history.dat ${DIR}/history.dat_prop
	cp band/step0000.bnd band/step0000.bnd_${m}_${islv}
    done
done
