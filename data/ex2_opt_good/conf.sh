gnuplot<<EOF
set term svg fsize 17
set out "step68conf_conv.svg"
# set xl 'x1'
# set yl 'x2'
set size ratio -1
set xr[0:1]
set yr[0:1]
p "step0068.bp_conv" w l lw 1 lc 0 notitle

# set term post eps enhanced color
# set size ratio -1
# set out "eps/cnfgr.$i.eps"

EOF
gnuplot<<EOF
set term svg fsize 17
set out "step68conf_prop.svg"
# set xl 'x1'
# set yl 'x2'
set size ratio -1
set xr[0:1]
set yr[0:1]
p "step0068.bp_prop" w l lw 1 lc 0 notitle

# set term post eps enhanced color
# set size ratio -1
# set out "eps/cnfgr.$i.eps"

EOF
