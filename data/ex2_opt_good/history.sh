gnuplot<<EOF
set term svg fsize 17
set out "history.svg"
set xl 'Step'
set yl 'Full bandgap width'
# set yr[0:1800]
p "history.dat_prop" w lp title "Proposed method","history.dat_conv" w lp title "Conventional method"

EOF
