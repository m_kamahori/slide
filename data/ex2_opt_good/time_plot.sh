gnuplot<<EOF
set term svg fsize 17
set out "time_ne.svg"
set xl 'Number of elements'
set yl 'Computational time [sec/step]'
set yr[0:1800]
p "time.dat_prop" title "Proposed method","time.dat_conv" title "Conventional method"

EOF
