gnuplot<<EOF
set term svg fsize 17
set out "step68band_conv.svg"
# set xl 'x1'
# set yl 'objective function'
set size ratio 1.2
# set xr[0:1]
# set yr[0:1]
p "step0068.bnd_conv" notitle,\
  "step0068.slc_conv" pt 4 notitle

# set term post eps enhanced color
# set size ratio -1
# set out "eps/cnfgr.$i.eps"

EOF
gnuplot<<EOF
set term svg fsize 17
set out "step68band_prop.svg"
# set xl 'x1'
# set yl 'objective function'
set size ratio 1.2
# set xr[0:1]
# set yr[0:1]
p "step0068.bnd_prop" notitle,\
  "step0068.slc_prop" pt 4 notitle

# set term post eps enhanced color
# set size ratio -1
# set out "eps/cnfgr.$i.eps"

EOF
