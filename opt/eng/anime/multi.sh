dir=png                         # png file directry
filename=test2.mp4            # output file name
fps=4                           # frame per second
end=054                          # end step
nframe=60                       # number of frames

i=0
mkdir $dir
rm ./${dir}/*

echo making history files.
for step in `seq -f %03g 0 $end`
do
    i=$((i+1))
    cat ./history/history.dat | head -$i > ./history/step${step}.hst
done

for step in `seq -f %03g 0 $nframe`
do
    echo ${step}
    if [ $step -le $end ];then
        num=$step
    else
        num=$end
    fi

    gnuplot <<EOF
# set term png small
set term png
set out "./${dir}/xxx_${step}.png"

set multiplot

##################band#####################
    set origin 0.5, 0.5
    set size 0.5, 0.5
    set yr[0:10]
p "./band/step0${num}.bnd" lc 6 notitle,"./evslct/step0${num}.slc" lc 7 pt 4 notitle
##################band#####################

#################history###################
    res
    set origin 0.0, 0.0
    set size 1.0, 0.5
set xl 'step'
set yl 'Full bandgap width'
p "./history/step${end}.hst" w l lt 0 notitle,"./history/step${num}.hst" w lp lt 1 notitle
#################history###################

#############configuration#################
    set origin 0.0, 0.5
    set size 0.5, 0.5
set xl 'x_1'
set yl 'x_2'
set size ratio -1
# set xr[0:1]
# set yr[0:0.87]
p "./bp/step0${num}.bp" lt 3 pt 7 ps 0.4 notitle # ここでデータファイル指定
#############configuration#################

unset multiplot
EOF
done


# pngからmp4を出力
ffmpeg -r ${fps} -i ./${dir}/xxx_%3d.png -c:v libx264 -pix_fmt yuv420p $filename

# mp4を再生
totem $filename &
