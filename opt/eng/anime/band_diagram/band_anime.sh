# dataのディレクトリで動く。

dir=png
filename=ex4band.mp4
fps=7
end=78

mkdir $dir
for step in `seq -f %02g 0 ${end}`
do
    gnuplot<<EOF
# set term svg fsize 17
set term png
set out "./${dir}/xxx_${step}.png" # xxx_はフォーマット
# set xl 'x1'
#set yl 'objective function'
# set size ratio -1
# set xr[0:1]
# set yr[0:1]
p "./band/step00${step}.bnd" lt 3 pt 1 notitle,"./evslct/step00${step}.slc" pt 3 lc 1 notitle

EOF
done

# pngからmp4を出力
ffmpeg -r ${fps} -i ./$dir/xxx_%2d.png -c:v libx264 -pix_fmt yuv420p $filename

# mp4を再生
totem $filename &
