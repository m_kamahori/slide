gnuplot<<EOF
set term svg fsize 17
set out "band.svg"
# set xl 'x1'
#set yl 'objective function'
set size ratio 1.5
# set xr[0:1]
# set yr[0:1]
# set logscale
p "step0012.bnd" notitle

# set term post eps enhanced color
# set size ratio -1
# set out "eps/cnfgr.$i.eps"

EOF
