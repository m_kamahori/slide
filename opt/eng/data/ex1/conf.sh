gnuplot<<EOF
set term svg fsize 17
set out "ex1step00bnd.svg"
# set xl 'x1'
#set yl 'objective function'
set size ratio 1.5
# set xr[0:1]
# set yr[0:1]
# set logscale
p "step0000.bnd" lc 6 notitle,"step0000.slc" lc 7 pt 4 notitle

# set term post eps enhanced color
# set size ratio -1
# set out "eps/cnfgr.$i.eps"

EOF
