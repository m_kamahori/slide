step=03
gnuplot<<EOF
set term svg fsize 17
set out "ex3step$step band.svg"
# set xl 'x1'
#set yl 'objective function'
# set size ratio -1
# set xr[0:1]
# set yr[0:1]
p "step00$step.bnd" lt 3 pt 1 notitle,"step00$step.slc" pt 3 lc 1 notitle

# set term post eps enhanced color
# set size ratio -1
# set out "eps/cnfgr.$i.eps"

EOF
