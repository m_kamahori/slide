gnuplot <<EOF
set term svg fsize 17
set out "l2err.svg"
set logscale
set grid ytics lc rgb "#bbbbbb" lw 1 lt 1
set grid mytics lc rgb "#bbbbbb" lw 1 lt 1
set grid xtics lc rgb "#bbbbbb" lw 1 lt 1
set grid mxtics lc rgb "#bbbbbb" lw 1 lt 1
p "l2err.dat" w lp ps 2 lw 2 notitle

EOF
