%% \documentclass[8pt,dvipdfmx,handout]{beamer} %プリント用
\documentclass[8pt,dvipdfmx]{beamer} %プレゼン用
%テーマ
%% なし	defaults, boxes, Bergen, Madrid, Pittsburgh, Rochester
%% 木構造	Antibes, JuanLesPians, Montpellier
%% 内容	Berkeley, PaloAlto, Goettingen, Marburg, Hannover
%% ミニフレーム	Berlin, Ilmenau, Dresden, Darmstadt, Frankfurt, Singapore, Szeged
%% セクション名	Copenhagen, Luebeck, Malmoe, Warsaw
\usetheme{Mizuki}


\setbeamertemplate{navigation symbols}{}
%% \setbeamertemplate{footline}[frame number] 
%% \setbeamertemplate{itemize items}[default]
\setbeamertemplate{items}[default]

\useoutertheme{infolines}

\setbeamercolor{block title}{bg=blue!50!gray, fg=white}
\setbeamercolor{block body}{bg=blue!10}

%数式
\usefonttheme{professionalfonts}

\usepackage{bm}
\usepackage{mathtools}
\usepackage{xparse}
%% \usepackage{graphicx}
\usepackage{hyperref}
\usepackage{movie15_dvipdfmx}
%% \usepackage{media9}
\usepackage{tikz}
\usepackage{setspace}
\usepackage{array}
\usepackage{ascmac}             %itembox
%% \usepackage[absolute, overlay]{textpos}
%% \usepackage{environ}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{kbordermatrix}


% caption に番号追加
%% \setbeamertemplate{caption}[numbered]

% 矢印の定義
\usetikzlibrary{shapes,arrows}
%% define fancy arrow. \tikzfancyarrow[<option>]{<text>}. ex: \tikzfancyarrow[fill=red!5]{hoge}
  \tikzset{arrowstyle/.style n args={2}{inner ysep=0.1ex, inner xsep=0.5em, minimum height=2em, draw=#2, fill=black!20, font=\sffamily\bfseries, single arrow, single arrow head extend=0.4em, #1,}}
  \NewDocumentCommand{\tikzfancyarrow}{O{fill=black!20} O{none}  m}{
    \tikz[baseline=-0.5ex]\node [arrowstyle={#1}{#2}] {#3 \mathstrut};}

\newcommand{\pd}[2]{\dfrac{\partial #1}{\partial #2}}
\def\e{\varepsilon}
\def\I{\mr{I}}
\def\D{\mr{D}}
\def\q{\mr{q}}
\newcommand{\m}[1]{\mathsf{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\wt}[1]{\widetilde{#1}}
\newcommand{\ks}[2]{{\mc S}^{#1}_{#2}}
\newcommand{\kd}[2]{{\mc D}^{#1}_{#2}}
\newcommand{\kst}[2]{\wt{{\mc S}}^{#1}_{#2}}
\newcommand{\kdt}[2]{\wt{{\mc D}}^{#1}_{#2}}
\newcommand{\bs}[1]{\boldsymbol{#1}}

\newenvironment{mybox}[2]
{\begin{beamercolorbox}
[wd=#1, sep=0pt, shadow=true, rounded=true]{#2}}
{\end{beamercolorbox}}

\def\mr#1{\mathrm{#1}}

%% \title[]{周期構造の固有値問題に対する境界要素法の$\mc{H}$-matrix法による高速化}
%% \title[]{境界要素法とSakurai--Sugiura法を用いた\\周期構造のトポロジー最適設計の高速化}
\title[]{An acceleration of the BEM+SSM eigensolver for periodic structures \\ by the $\mc{H}$-matrix method}
%% \title[]{A topology optimisation for hexagonal lattice with the Sakurai-Sugiura method and the boundary element method}
%% \title[]{2次元六方格子からなる周期構造のトポロジー最適化}
%% cmd2016:2次元Helmholtz方程式の2周期境界値問題における\\トポロジー最適化について

\author[Mizuki KAMAHORI]{Mizuki KAMAHORI}
%% \date[9 Oct. 2016 \textbar ~Keio univ.]{9 Oct. 2016 \textbar ~Keio Univ.}
\date[\today]{\today}
\institute[NU]{Nagoya Univ.}

\begin{document}

\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Backgrounds}
\subsection{Full bandgap}

\begin{frame}{\insertsection\small~~~-\insertsubsection}

  \vspace{-5pt}
  \begin{definition}
    \alert{Full} bandgap:

    frequency range in which waves cannot propagate in \alert{any} direction.
  \end{definition}


  \vspace{10pt}
  \begin{minipage}{0.5\textwidth}
    Applications:
    \begin{itemize}
    \item {\color<3->{blue}Photonic} crystal (exhibit the bandgap for electromagnetic waves)
      \begin{itemize}
      \item laser, waveguide, slow light, super lens, cloaking, etc...
      \end{itemize}
    \item Phononic crystal (exhibit the bandgap for acoustic/elastic waves)
      \begin{itemize}
      \item acoustic filter, noise/vibration control, transducer, etc...
      \end{itemize}
    \end{itemize}
  \end{minipage}
  \begin{minipage}{0.4\textwidth}
    \centering{
      \includegraphics[scale=0.7]{../../fig/image002.eps}

      {\small Photonic crystal wave guide.}

      {\tiny http://www.baba-lab.ynu.ac.jp/pbwgj.htm}
    }
  \end{minipage}

  It is necessary to...

  \pause
  \begin{center}
    \begin{minipage}{0.8\hsize}
      \begin{mybox}{\hsize}{block body}
        find optimal shape of periodic structures which maximize full bandgap width.
        %% make full bandgap of periodic structures larger with the topology optimisation.    
      \end{mybox}
    \end{minipage}
  \end{center}
  %% We study about the Phononic crystal.

  %% To make the matter easier, we consider about only out-of-plane displacement.
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Previous studies}

\begin{frame}{\insertsection\small~~~-\insertsubsection}

  \begin{block}{\insertsubsection}
    \begin{enumerate}
    \item<2-> A topology optimisation for \alert{non-periodic} structures with the boundary element method and the Sakurai--Sugiura method\\
     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ---{\it Kitabayashi, Isakari et al, 2015}
    \item<3-> A topology optimisation for \alert{periodic} structures with the BEM and the SSM\\
     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ---{\it Kamahori, Isakari et al, 2016}
    \end{enumerate}
  \end{block}

  \onslide<4->
  However, the optimisation costs a lot of time to obtain optimal configuration. (about 1h/step)

  \onslide<5->

  In the optimisation algorithm, \alert{the periodic band diagram} calculation part is the most time consuming. 

  \onslide<6->

  \begin{alertblock}{Scope}
    Acceleration of the algorithm for the periodic band diagram calculation.
  \end{alertblock}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{A method of periodic band diagram calculation}

\subsection{The eigenvalue problem of periodic structures}

\begin{frame}{\insertsection\small~~~-\insertsubsection}

  The eigenvalue problem of periodic problem is defined as follows:
  \begin{block}{\insertsubsection}
    %% \vspace{-10pt}
    \begin{minipage}{0.4\textwidth}
      \begin{align*}
        &\text{Find $\omega$ with which a non-trivial solution }\\
        &\text{$u(x)$ exists such that}\\
        &\text{PDE:~}\\
        &~~~~~~\nabla^2 u(x)+\e\omega^2u(x)=0&x\in U\cap\Omega\\
        &\text{homogeneous boundary conditions:}\\
        %% &~~~~~~u(x)=\alert{0}&x\in\Gamma_\mr{u}\\
        &~~~~~~q(x)=\pd{u(x)}{n}=\alert<1>{0}&x\in\Gamma_\mr{q}\\
        &\text{quasi-periodic boundary conditions:}\\
        &~~~~~~u(x+a_i)=u(x)\exp(i\alert<2>{K}\cdot a_i)&x\in\Gamma_{\mr{I}}\\
        &~~~~~~\pd{u}{x_i}(x+a_i)=\pd{u}{x_i}(x)\exp(i\alert<2>{K}\cdot a_i)&x\in\Gamma_{\mr{I}}
        %% (C_1^2-C_2^2)\tilde u_{j,jk}(\bm x,\omega)+C_2^2\tilde u_{k,jj}(\bm x,\omega)+\omega^2\tilde u_k(\bm x,\omega)=0
      \end{align*}
    \end{minipage}
    \begin{minipage}{0.4\textwidth}
      %% \vspace{10pt}
      \centering{
        \includegraphics[scale=0.64]{../../fig/unitcell2.png}

        ~~The unit cell of a periodic structure.
      }
    \end{minipage}
  \end{block}

  \begin{tabular}{lcllcl}
    $\varepsilon$ & : & Dielectric constant & $a_i\in\mathbb{R}^2$          & : & Lattice vector\\
    $\omega$      & : & Angular frequency   & $K=(K_1,K_2)^t\in\mathbb{R}^2$ & : & Bloch wave vector
  \end{tabular}
  %% where $\varepsilon$ and $\omega$ are the dielectric constant and the angular frequency, respectively.
  %% $a_i\in\mathbb{R}^2$ and $K=(K_1,K_2)^t\in\mathbb{R}^2$ are the lattice vector and the Bloch wave vector, respectively.

  %% By the discretization, the eigenvalue problem to solve turns into finding $\omega$ with which a non-trivial solution $u(\bm x)$ exists.


\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The reduced first Brillouin zone}

\begin{frame}{\insertsection\small~~~-\insertsubsection}

  By solving the eigenvalue problem for all $K$ on the edge of the reduced first Brillouin zone, we can obtain the band diagram of periodic structures.

  \vspace{10pt}
  \begin{minipage}{0.4\textwidth}

    ~~~
    \centering{
      \includegraphics[clip,scale=0.5]{../../fig/firstbz_hex.eps}

      %% ~~~~~~~~~~~~{The reduced first Brillouin zone.}
    }
  \end{minipage}
  \begin{minipage}{0.1\textwidth}
    \pause
    \centering{
      \tikzfancyarrow[minimum height=15pt,rotate=0, fill=black!40]{}
    }
  \end{minipage}
  \begin{minipage}{0.4\textwidth}

    %% \vspace{10pt}
    \includegraphics[clip,scale=0.4]{../../fig/fullbandgap.eps}

    %% \centering{
    %% ~~~~{The band diagram.}
    %% }
  \end{minipage}

  %% \pause
  %% Several numerical methods for the full bandgap calculations have been proposed.
  %% {\scriptsize
  %%   \begin{block}{{\small Existing Methods}}
  %%     \begin{itemize}
  %%     \item Plane Wave Expansion (PWE) $\longrightarrow$ simple domain only
  %%     \item Multiple scattering method (MST) $\longrightarrow$ simple domain only
  %%     \item Finite-Difference Time-Domain method (FDTD)$\cdots$ domain type solver, time domain $\longrightarrow$ high cost
  %%     \item FEM$\cdots$ domain type solver $\longrightarrow$ high cost
  %%     \end{itemize}
  %%   \end{block}
  %% }

  %% \pause
  %% How about \alert{BEM}?

  %% $\longrightarrow$ applicable to complex domain, boundary type solver, frequency domain.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Sakurai-Sugiura method~(SSM)}

\begin{frame}{\insertsection\small~~~-\insertsubsection}

  %% The boundary integral equation can be discretised by the collocation
  %% method into the following non-linear algebraic equations:
  \small
  \begin{center}
    \begin{minipage}{0.3\hsize}
      \begin{mybox}{\hsize}{block body}
        \centering Periodic eigenvalue problem
      \end{mybox}
    \end{minipage}
  \end{center}
  \pause

  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  \tikzfancyarrow[minimum height=10pt,rotate=-90, fill=black!40]{}
  \begin{minipage}{0.4\hsize}
    Reformulation by the BEM
  \end{minipage}
  \pause

  \begin{block}{The boundary integral equation}
    \vspace{-11pt}
    \begin{align*}
      c(x)u(x)+({\color<4>{red}{\mc D}}_{\Gamma_{\mr q}}u)(x)
      &+\sum_{k=1}^2({\color<4>{red}{\mc D}}_{\Gamma_{\mr{I}_k}}u)(x)
      +\sum_{k=1}^2({\color<4>{red}\wt{{\mc D}}}_{\Gamma_{\mr{D}_k}}u)(x)
      \nonumber\\
      %% -({\mc S}_{\Gamma_u}q)(x)
      &-\sum_{k=1}^2({\color<4>{red}{\mc S}}_{\Gamma_{\mr{I}_k}}q)(x)
      +\sum_{k=1}^2({\color<4>{red}\wt{{\mc S}}}_{\Gamma_{\mr{D}_k}}q)(x)=0
    \end{align*}

    {\scriptsize
      \begin{tabular}{ll}
        $({\color{red}{\mc S}}_{\Gamma}q)(x)=\int_{y\in\Gamma}G(x-y)q(y)dS(y)$
        &$({\color{red}\wt{{\mc S}}}_{\Gamma_{\mr{D}_k}}q)(x)=\int_{y\in\Gamma_{\mr{D}_k}}G(x-y)\exp(iK\cdot a_k)q(y-a_k)dS(y)$\\
        $({\color{red}{\mc D}}_{\Gamma}u)(x)=\int_{y\in\Gamma}\pd{G(x-y)}{n_y}u(y)dS(y)$
        &$({\color{red}\wt{{\mc D}}}_{\Gamma_{\mr{D}_k}}u)(x)=\int_{y\in\Gamma_{\mr{D}_k}}\pd{G(x-y)}{n_y}\exp(iK\cdot a_k)u(y-a_k)dS(y)$
      \end{tabular}
    }
  \end{block}

  \pause
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  \tikzfancyarrow[minimum height=10pt,rotate=-90, fill=black!40]{}
  \begin{minipage}{0.4\hsize}
    Discretisation by the collocation method
  \end{minipage}

  \pause
  \begin{align*}
    %% \kbordermatrix{
    %%   \mc{D}^{\mr q}_{\mr q}+\frac{I}{2}&    m  &        &    n    \\
    %%   m & \m{B} & \vrule & \m{D}(K)\\ \cline{2-4}
    %%   n & \m{C} & \vrule & \m{E}(K)
    %% }
    \left[
      \begin{array}{ccccc}
        \kd{\q}{\q}+\frac{\m I}{2} & \vrule & \kst{\q}{\D_k}-\ks{\q}{\I_k} & \vrule & \kdt{\q}{\q}-\kd{\q}{\I_k}\\ \hline
        \kd{\I_k}{\q}           & \vrule & \kst{\I_k}{\D_k}-\ks{\I_k}{\I_k} & \vrule & \kdt{\I_k}{\D_k}-\kd{\I_k}{\I_k}+\frac{\m I}{2}\\ \hline
        \kd{\D_k}{\q}           & \vrule & \kst{\D_k}{\D_k}-\ks{\D_k}{\I_k} & \vrule & \kdt{\D_k}{\D_k}-\kd{\D_k}{\I_k}+\frac{\wt{\m I}}{2}
      \end{array}
      \right]
    \left[
      \begin{array}{c}
        u^{ }_\q\\ \hline
        q^{ }_{\I_k}\\\hline
        u^{ }_{\I_k}
      \end{array}
      \right]
    =\bm{0}
  \end{align*}

  We henceforth denote this equation as $\m{A}(\omega)\psi=\bm{0}$.

  Thus, the periodic eigenvalue problem is now turned into finding $\omega$ with which a non-trivial solution $\psi$ exists.


\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% \subsection{A topology optimisation for a periodic sturcture}

%% \begin{frame}{\insertsection\small~~~-\insertsubsection}

%%   \centering
%%   \includemovie[label=history,poster=../../fig/xxx_020.png]{320pt}{240pt}{./mp4/ex4.mp4}
  
%% \end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{An acceleration of forward problem}

\subsection{Target}

\begin{frame}{\insertsection\small~~~-\insertsubsection}

  \begin{exampleblock}{Sakurai--Sugiura Method~(SSM)~[Sakurai and Sugiura, 2003; Asakura et al., 2009]}
    \begin{minipage}{0.7\hsize}
      The eigenvalues of $A(\omega)\psi=0$ inside a preset closed curve
      $\gamma$ in $\mathbb{C}$ are equivalent to those of the pencil
      $H_m^<-\lambda H_m$,       
      {\small
        \begin{align*}
          &H_m=[\mu_{i+j-2}]_{i,j=1}^m~~,~~H_m^<=[\mu_{i+j-1}]_{i,j=1}^m
          \\ &\text{where~}\mu_j=\frac{1}{2\pi\mr{i}}\int_{z\in\gamma}
          z^j f(z) dz\in\mathbb{C}^{\ell\times\ell},~f(z)=\mathsf{U}^H
          \alert<2->{\mathsf{A}(z)^{-1}\mathsf{V}}\in\mathbb{C}^{\ell\times\ell}
        \end{align*}
        %% \begin{align*}
        %%   &H_m=\begin{pmatrix}
        %%   \mu_0     & \mu_1  & \cdots & \mu_{m-1} \\
        %%   \mu_1     & \mu_2  & \cdots & \mu_{m}   \\
        %%   \vdots    & \vdots & \ddots & \vdots    \\
        %%   \mu_{m-1} & \mu_m  & \cdots & \mu_{2m-2}
        %%   \end{pmatrix},~
        %%   H_m^<=\begin{pmatrix}
        %%   \mu_1   & \mu_2     & \cdots & \mu_{m}  \\
        %%   \mu_2   & \mu_3     & \cdots & \mu_{m+1}\\
        %%   \vdots  & \vdots    & \ddots & \vdots    \\
        %%   \mu_{m} & \mu_{m+1} & \cdots & \mu_{2m-1}
        %%   \end{pmatrix}
        %%   \\
        %%   &\text{where~}\mu_j=\frac{1}{2\pi\mr{i}}\int_{z\in\gamma} z^j f(z) dz\in\mathbb{C}^{\ell\times\ell},~f(z)=\mathsf{U}^H \alert<2->{\mathsf{A}(z)^{-1}}\mathsf{V}\in\mathbb{C}^{\ell\times\ell}
        %% \end{align*}
      }
    \end{minipage}
~
    \begin{minipage}{0.20\hsize}
      \begin{figure}
        \centering
        \includegraphics[clip,scale=0.36]{../../fig/ss2.eps}
      \end{figure}
    \end{minipage}
  \end{exampleblock}
  \pause
  \begin{mybox}{\hsize}{block body alerted}
    Thus, the \textcolor{red}{non-linear eigenvalue problem} is
    converted into the \textcolor{red}{generalised eigenvalue problem}
    which is tractable with conventional eigensolvers.
  \end{mybox}

  \pause
  \vspace{9pt}
  %% \onslide<2->{
  \begin{minipage}{0.35\textwidth}
    \begin{itembox}[c]{Algorithm}
      \begin{algorithmic}                  
        \FOR{$i=1$ \TO{\color<3->{blue}$I$}}
        %% \STATE $K \leftarrow K_i$
        \FOR{$j=1$ \TO{\color<3->{blue}$J$}}
        %% \STATE $z\leftarrow z_j$
        \STATE $\mathsf{X}\leftarrow\alert<2->{\mathsf{A}(z_i, K_j)^{-1}\mathsf{V}}$
        \ENDFOR
        \ENDFOR
      \end{algorithmic}
    \end{itembox}

  $I$:~The number of integral points on $\gamma$

  $J$:~The number of wavevector $K$
  \end{minipage}
  %% }
  \begin{minipage}{0.59\textwidth}
    \begin{itemize}
    \item We have to solve linear algebraic eq. $\mathsf{A}(z, K)\mathsf{X}=\mathsf{V}$ for {\color<3->{blue}$I\times J$} times.
    \item The computational cost is \alert{$\mc{O}(N^3)$}．
    \end{itemize}
    \onslide<4->

    \centering\tikzfancyarrow[minimum height=10pt,rotate=-90, fill=black!40]{}
    \begin{center}
      \begin{minipage}{0.8\hsize}
        \begin{mybox}{\hsize}{block body}
          \centering The target to be accelerated is \alert{$\mathsf{A}(z, K)^{-1}\mathsf{V}$}．
        \end{mybox}
      \end{minipage}
    \end{center}
  \end{minipage}


\end{frame}

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Proposed method}

\begin{frame}{\insertsection\small~~~-\insertsubsection}

  How can we reduce the computational cost of $\mathsf{A}(z, K)^{-1}\mathsf{V}$ ?
  %% Before applying the $\mc{H}$-matrix method to our algorithm, we represent the algebraic equation  $\m{A}(z)\m{X}=\m{V}$ as follows:
%% The matrix $\m{A}(K, z)$ can be devided as follows:
%% \begin{block}{Conventional method}
%%   Solving the linear algebraic eq. $\mathsf{A}(z, K)\mathsf{X}=\mathsf{V}$ with the conventional LU decomposition.
%% \end{block}
%% \pause
%% \begin{minipage}{0.4\hsize}
  \pause
  \begin{align*}
    %% \kbordermatrix{
    %%   \mc{D}^{\mr q}_{\mr q}+\frac{I}{2}&    m  &        &    n    \\
    %%   m & \m{B} & \vrule & \m{D}(K)\\ \cline{2-4}
    %%   n & \m{C} & \vrule & \m{E}(K)
    %% }
    \m{A}(K, z)=
    \left[
      \begin{array}{ccccc}
        \kd{\q}{\q}+\frac{\m I}{2} & \vrule & \kst{\q}{\D_k}-\ks{\q}{\I_k} & \vrule & \kdt{\q}{\q}-\kd{\q}{\I_k}\\ \hline
        \kd{\I_k}{\q}           & \vrule & \kst{\I_k}{\D_k}-\ks{\I_k}{\I_k} & \vrule & \kdt{\I_k}{\D_k}-\kd{\I_k}{\I_k}+\frac{\m I}{2}\\ \hline
        \kd{\D_k}{\q}           & \vrule & \kst{\D_k}{\D_k}-\ks{\D_k}{\I_k} & \vrule & \kdt{\D_k}{\D_k}-\kd{\D_k}{\I_k}+\frac{\wt{\m I}}{2}
      \end{array}
      \right]\\
    \coloneqq
    \left[
      \begin{array}{ccccc}
        ~~\alert<3->{\m{B}(z)}~~ & \vrule & \multicolumn{3}{c}{~~~~~~~~~~~~~~~~~\raisebox{0pt}[0pt]{$\m{D}(z, K)$}~~~~~~~~~~~~~~~~~}\\ \hline
              & \vrule & & &\\
        \raisebox{4.5pt}[0pt]{\alert<3->{$\m{C}(z)$}} & \vrule & \multicolumn{3}{c}{\raisebox{4.5pt}[0pt]{$\m{E}(z, K)$}}
      \end{array}
      \right]
  \end{align*}

  \pause
  $\alert{\m{B(z)}}$, $\alert{\m{C(z)}}$ are independent from $K$.

  \pause
  \tikzfancyarrow[minimum height=10pt,rotate=0, fill=black!40]{}
  Once you compute $\m{B(z)}$ and $\m{C(z)}$, you can reuse them for all wavevector $K$.

%% \onslide<5->{
%% Once we LU decompose the submatrix \alert{$\m{B}$}, we can reuse the LU
%% factorized matrices in the loop of wave vector $K$ because \alert{$\m{B}$} is independent from $K$.
%% }

\end{frame}
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Woodbury formula}

\begin{frame}{\insertsection\small~~~-\insertsubsection}
  In many cases, the size of $\m{B}(z)$ is larger than the other matrices.

  \begin{minipage}{0.25\hsize}
    \centering
    \includegraphics[clip,scale=1]{../../fig/block.eps}
  \end{minipage}
  \onslide<2->{
  \begin{minipage}{0.15\hsize}
    \tikzfancyarrow[minimum height=10pt,rotate=0, fill=blue!40]{solve}
  \end{minipage}
  \begin{minipage}{0.5\hsize}
    \only<2>{
    \includegraphics[clip,scale=1]{../../fig/block.1.eps}
    }
    \only<3->{
    \includegraphics[clip,scale=1]{../../fig/block.2.eps}
    }
  \end{minipage}
  }

  \onslide<3->{
  In this case, you can further reduce the computational cost with the use of the Woodbury formula.
  \begin{exampleblock}{Woodbury formula}
    \centering
    \includegraphics[clip,scale=0.9]{../../fig/woodbury.eps}
  \end{exampleblock}
  }

  \onslide<4->{
  By substituting the Woodbury formula into the algebraic equation, we obtain
  }

  \onslide<5->{
  \centering
  \includegraphics[clip,scale=1]{../../fig/block1.1.eps}
  }
\end{frame}
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\insertsection\small~~~-\insertsubsection}
\input{woodbury}
\end{frame}
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Algorithm}

\begin{frame}{\insertsection\small~~~-\insertsubsection}
  \begin{algorithmic}                  
    \FOR{$i=1$ \TO $I$}
    \item build~~~~~~~~\,\, $\m{B}(z_i),~\m{C}(z_i)$
    \item LU factorize $\m{B}(z_i)$
    \item compute~~~~\, $\m{CB}^{-1}(z_i)$
    \FOR{$j=1$ \TO $J$}
    \item build~~~~~~~~\,\, $\m{D}(z_i, K_j),~\m{E}(z_i, K_j)$
    %% \STATE $x\leftarrow\m{B}^{-1}\{\m{I}+\m{D}(\m{E}-\m{C}\m{B}^{-1}\m{D})^{-1}\m{C}\m{B}^{-1}\}(f-\m{D}\m{E}^{-1}g)$
    %% \STATE $y\leftarrow\m{E}^{-1}(g-\m{C}x)$
      %% \only<2>{
      \begin{figure}
        \flushleft
        \includegraphics[clip,scale=1.0]{../../fig/block1.1.eps}
      \end{figure}
      %% }
    \ENDFOR
    \ENDFOR
  \end{algorithmic}

\onslide<2->{
  \begin{block}{Proposed methods}
    \begin{enumerate}
    \item Solving $\mathsf{A}(z, K)^{-1}\mathsf{V}$ by the Woodbury formula.
    %% \item Precomputing $\alert{\m{B^{-1}}}$ and $\alert{\m{CB^{-1}}}$ with the LU decomposition.
    \item<3-> Applying the ${\mc H}$-matrix method to operations which are relevant to $\m{B}$ and $\m{C}$ in the Woodbury formula.
    \item<4-> Applying the ${\mc H}$-matrix method to all operations in the Woodbury formula.
    \item<5-> Applying the ${\mc H}$-matrix method to the operation of $\mathsf{A}(z, K)^{-1}\mathsf{V}$.
    \end{enumerate}
  \end{block}
}
\end{frame}
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Tuning for parallel computing}

\begin{frame}{\insertsection\small~~~-\insertsubsection}
\begin{minipage}{0.48\hsize}
\small
We measured computational time for each integral points.

  \begin{algorithmic}                  
    \FOR{$i=1$ \TO $I$}
  \item /*=====start=====*/
  \item build~~~~~~~~\,\, $\m{B}(z_i),~\m{C}(z_i)$
  \item LU factorize $\m{B}(z_i)$
  \item compute~~~~\, $\m{CB}^{-1}(z_i)$
    \FOR{$j=1$ \TO $J$}
  \item build~~~~~~~~\,\, $\m{D}(z_i, K_j),~\m{E}(z_i, K_j)$
    \STATE $x\leftarrow\m{B}^{-1}\{\m{I}+\m{D}(\m{E}-\m{C}\m{B}^{-1}\m{D})^{-1}\m{C}\m{B}^{-1}\}(f-\m{D}\m{E}^{-1}g)$
    \STATE $y\leftarrow\m{E}^{-1}(g-\m{C}x)$
    \ENDFOR
  \item /*===== end =====*/
    \ENDFOR
  \end{algorithmic}

  \vspace{9pt}

  The larger real part of an integral point is, the more computational time it costs.

  \begin{minipage}{\hsize}
    \centering
    \tikzfancyarrow[minimum height=10pt,rotate=-90, fill=black!40]{}
  \end{minipage}

  We tuned our algorithm to compute from those integral points.
\end{minipage}
  \begin{minipage}{0.48\hsize}
    \centering
    \includegraphics[clip,scale=0.3]{../../fig/skbn_en.eps}

    \includegraphics[clip,scale=0.3]{../../fig/time_iskbn_bp.eps}
  \end{minipage}

\end{frame}

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% \subsection{${\mc H}$-matrix method}

%% \begin{frame}{\insertsection\small~~~-\insertsubsection}

%%   {\Large\underline{Overview of ${\mc H}$-matrix method}}

%%   \begin{enumerate}
%%   \item Clustering of a boundary
%%   \item Blocking of the coefficient matrix corresponding to the clusters in consideration of the admissibility condition.
%%   \item Approximation of sub matirices (far-field infuluences)
%%   \end{enumerate}

%%   \begin{block}{Admissibility condition}
%%     A block matrix is admissible, if its row cluster $C_i$ and column cluster $C_j$ satisfy following admissibility condition.
%%     \begin{align*}
%%       \min{\{\mr{diam}C_i,~\mr{diam}C_j\}}\le\eta\mr{dist}\{C_i,~C_j\}
%%     \end{align*}
%%   \end{block}

%%   \begin{figure}
%%     \centering
%%     \includegraphics[clip,scale=1]{../../fig/admis1.eps}
%%   \end{figure}

%% \end{frame}


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \begin{frame}{\insertsection\small~~~-\insertsubsection}
%%   \begin{align*}
%%     \m{A}(K, z)=
%%     \left[
%%       \begin{array}{ccccc}
%%         \kd{\q}{\q}+\frac{\m I}{2} & \vrule & \alert<2->{\kst{\q}{\D_k}-\ks{\q}{\I_k}} & \vrule & \alert<2->{\kdt{\q}{\D_k}-\kd{\q}{\I_k}}\\ \hline
%%         \kd{\I_k}{\q}           & \vrule & \alert<2->{\kst{\I_k}{\D_k}-\ks{\I_k}{\I_k}} & \vrule & \alert<2->{\kdt{\I_k}{\D_k}-\kd{\I_k}{\I_k}}+\frac{\m I}{2}\\ \hline
%%         \kd{\D_k}{\q}           & \vrule & \alert<2->{\kst{\D_k}{\D_k}-\ks{\D_k}{\I_k}} & \vrule & \alert<2->{\kdt{\D_k}{\D_k}-\kd{\D_k}{\I_k}}+\frac{\wt{\m I}}{2}
%%       \end{array}
%%       \right]\\
%%     \coloneqq
%%     \left[
%%       \begin{array}{ccccc}
%%         ~~\m{B}(z)~~ & \vrule & \multicolumn{3}{c}{~~~~~~~~~~~~~~~~~\raisebox{0pt}[0pt]{$\m{D}(z, K)$}~~~~~~~~~~~~~~~~~}\\ \hline
%%         & \vrule & & &\\
%%         \raisebox{4.5pt}[0pt]{$\m{C}(z)$} & \vrule & \multicolumn{3}{c}{\raisebox{4.5pt}[0pt]{$\m{E}(z, K)$}}
%%       \end{array}
%%       \right]
%%   \end{align*}

%%     To build ${\mc H}$-matrix of $\m{D}$ and $\m{E}$, we have to add \alert<2->{two matrices which have different column clusters}.

%% \pause
%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%% \tikzfancyarrow[minimum height=10pt,rotate=-90, fill=black!40]{}

%%     So, we have to build two matrices which have the same structures by the use of an admissibility condition for the periodic problem.
%% \end{frame}
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \subsection{An admissibility condition for the periodic problem}

%% \begin{frame}{\insertsection\small~~~-\insertsubsection}

%%   \begin{block}{\insertsubsection}
%%     A block matrix is admissible, if its row cluster $C_i$ and column cluster $C_j$ satisfy following admissibility condition.
%%     \begin{align*}
%%       \min{\{\mr{diam}C_i,~\mr{diam}C_j\}}\le\eta\min{\{\mr{dist}\{C_i,~C_j\},~\mr{dist}\{C_i,~C_k\}\}}
%%     \end{align*}
%%   \end{block}

%%   \begin{figure}
%%     \centering
%%     \includegraphics[clip,scale=1]{../../fig/admis2.eps}
%%   \end{figure}

%%   With this admissibility condition, you can obtain two ${\mc H}$-matrices which have the same structure.
%% \end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical example}

\subsection{Settings}

\begin{frame}{\insertsection\small~~~-\insertsubsection}

We exammined proposed method $1$--$4$ with this configuration whose number of elements on $\Gamma_\q$ is about two times larger than on $\Gamma_{\I\cup\D}$.

\begin{minipage}{0.4\hsize}
  \begin{block}{\insertsubsection}
        \begin{itemize}
        \item The number of wavevectors: $29$
        \item The number of integral points: $128$
        \end{itemize}
  \end{block}
\end{minipage}
~~~~~~~
\begin{minipage}{0.5\hsize}
  \centering{
    \includegraphics[scale=0.8]{../../fig/benchmark_conf2.eps}

    %% \includegraphics[scale=0.3]{../../fig/validation_band.eps}
  }
\end{minipage}

\underline{Environment}
\begin{itemize}
  \item Intel(R) Xeon(R) CPU E5-2690 v3 @ 2.60GHz (24 cores）
  \item Memory: $265$ GB
\end{itemize}

\begin{mybox}{\hsize}{block body alerted}
  We measured computational time to obtain the band diagram, changing the number of elements.
\end{mybox}

\end{frame}
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Result}

\begin{frame}{\insertsection\small~~~-\insertsubsection}

  \centering{
    %% \includegraphics[scale=0.4]{../../fig/bandopt_vs_bandopt_kai_vs_hbandopt.eps}
    \includegraphics[scale=0.4]{../../fig/time_ne_rad0.4.eps}
  }

  \begin{itemize}
  \item Computational time of proposed method $2$ was the least for $400$--$8,000$ elements, and that of proposed method $3$ was the least for over $8,000$ elements.
  \end{itemize}
  
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \section{Numerical example $2$}

%% \subsection{Settings}

%% \begin{frame}{\insertsection\small~~~-\insertsubsection}

%% We exammined proposed method 1--3 by computing the same band structure.

%% \begin{minipage}{0.4\hsize}
%%   \begin{block}{\insertsubsection}
%%         \begin{itemize}
%%         \item The number of wavevectors: $29$
%%         \item The number of integral points: $128$
%%         \end{itemize}
%%   \end{block}
%% \end{minipage}
%% ~~~~~~~
%% \begin{minipage}{0.5\hsize}
%%   \centering{
%%     \includegraphics[scale=0.8]{../../fig/benchmark_conf1.eps}

%%     %% \includegraphics[scale=0.3]{../../fig/validation_band.eps}
%%   }
%% \end{minipage}

%% \underline{Environment}
%% \begin{itemize}
%%   \item Intel(R) Xeon(R) CPU E5-2690 v3 @ 2.60GHz (24 cores）
%%   \item Memory: $265$ GB
%% \end{itemize}

%% \begin{mybox}{\hsize}{block body alerted}
%%   We measured computational time to obtain the band diagram, changing the number of elements.
%% \end{mybox}

%% \end{frame}
%% %% %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% \subsection{Result}

%% \begin{frame}{\insertsection\small~~~-\insertsubsection}

%%   \centering{
%%     %% \includegraphics[scale=0.4]{../../fig/bandopt_vs_bandopt_kai_vs_hbandopt.eps}
%%     \includegraphics[scale=0.4]{../../fig/time_rad0.1.eps}
%%   }

%%   \begin{itemize}
%%   \item Computational time of proposed method $2$ was the least for $200$--$2,000$ elements, and that of proposed method $3$ was the least for over $2,000$ elements.
%%   %% \item Computational time of proposed method $2$ was least than that of
%%   %%   the other.
%%   \end{itemize}
  
%% \end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}

\begin{frame}{\insertsection}

  \begin{itemize}
  \item We applied the $\mc{H}$-matrix method to all coefficient block matrices.
  %% \item We successfully reduced computational time of the periodic band diagram. 
    \pause
  \item We successfully accelerated SSM + BEM algorithm by the H-matrix method.
  \end{itemize}


  \vspace{20pt}

  \pause

  \begin{block}{Future tasks}
    \begin{itemize}

    \item Improve our algorithm to deal with the case that
      non-periodic boundary across the periodic boundary.

    \item Develop an optimization algorithm for electromagnetic waves of TM mode.

    \item Develop Topology optimisation coupled with shape optimisation.

    %% \item Checking the performance of the three methods in a case that has more elements.
    \end{itemize}
  \end{block}
  
\end{frame}
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \section{付録A}
%% \subsection{Reformulation by the BEM}

%% \begin{frame}{\insertsection  \small~~~-\insertsubsection}

%%   {\scriptsize
%%     With the BEM, the eigenvalue problem can be reformulated with the following boundary integral equations:
%%     {\color<2,5>{blue}
%%       \begin{align*}
%%         &c(x)u(x)
%%         +({\color<3>{red}{\mc D}}_{\Gamma_{\mr q}}u)(x)
%%         +\sum_{k=1}^2({\color<3>{red}{\mc D}}_{\Gamma_{\mr{I}k}}u)(x)
%%         \nonumber\\
%%         &+\sum_{k=1}^2({\color<3>{red}\wt{{\mc D}}}_{\Gamma_{\mr{D}k}}u)(x)
%%         %% -({\mc S}_{\Gamma_u}q)(x)
%%         -\sum_{k=1}^2({\color<3>{red}{\mc S}}_{\Gamma_{\mr{I}k}}q)(x)
%%         +\sum_{k=1}^2({\color<3>{red}\wt{{\mc S}}}_{\Gamma_{\mr{D}k}}q)(x)=0
%%       \end{align*}
%%     }
%%     where ${\color<3>{red}{\mc S}}, {\color<3>{red}{\mc D}}, {\color<3>{red}\wt{{\mc S}}}_{\Gamma_{\mr{D} i}}$
%%     and ${\color<3>{red}\wt{{\mc D}}}_{\Gamma_{\mr{D} i}}$ are integral operators defined as follows:
%%     \begin{align*}
%%       ({\color<3>{red}{\mc S}}_{\Gamma}q)(x)=&\int_{y\in\Gamma}{\color<4>{red}G(x-y)}q(y)dS(y)\\
%%       ({\color<3>{red}{\mc D}}_{\Gamma}u)(x)=&\int_{y\in\Gamma}\pd{{\color<4>{red}G(x-y)}}{n_y}u(y)dS(y)\\
%%       ({\color<3>{red}\wt{{\mc S}}}_{\Gamma_{\mr{D} i}}q)(x)=&
%%       \int_{y\in\Gamma_{\mr{D} i}}{\color<4>{red}G(x-y)}\exp(iK\cdot a_i)q(y-a_i)dS(y),~~~i=1,2\\
%%       ({\color<3>{red}\wt{{\mc D}}}_{\Gamma_{\mr{D} i}}u)(x)=&
%%       \int_{y\in\Gamma_{\mr{D} i}}\pd{{\color<4>{red}G(x-y)}}{n_y}\exp(iK\cdot a_i)u(y-a_i)dS(y),~~~i=1,2  
%%     \end{align*}
%%     where G(x-y) is the Green function for the Helmholtz equation in 2D, which has the following representation:
%%     \begin{align*}
%%       {\color<4>{red}G(x-y)}=\frac{i}{4}H_0^{(1)}\left(k|x-y|\right)
%%     \end{align*}
%%     where $H_n^{(1)}$ is the Hankel function of the first kind and order $0$, which includes $k$ non-linearly.

%%     Our eigenvalue problem is now defined as to

%%     \center{
%%       {\normalsize ``find $k$ with which {\color<5>{blue}the integral equation} has a non-trivial solution $u$''}.
%%     }

%%   }

%% \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
