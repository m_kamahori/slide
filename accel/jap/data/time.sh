gnuplot<<EOF
set term svg fsize 17
set out "time_rad0.4.svg"
# set xl 'x1'
#set yl 'objective function'
# set size ratio -1
# set xr[0:1]
# set yr[0:1]
set logscale
p "log.dat_wood_rad0.4" u 3:2 w lp notitle,\
  "log.dat_hbmat_rad0.4" u 3:2 w lp notitle,\
  "log.dat_hwood_rad0.4" u 3:2 w lp notitle

# set term post eps enhanced color
# set size ratio -1
# set out "eps/cnfgr.$i.eps"

EOF
